#!/usr/bin/env python3

"""
Puts a directory tree into a README.md in each folder.

TODO:
	- Detect if the old and new trees are identical instead of always rewriting
	  the old one
	- Preserve comments
	- Fix for files without '\n' in the end
	- Option to exclude some directories from the tree output
	- (it would be better to operate on lines, not strings)
"""

import argparse
import subprocess
from pathlib import Path

# DEBUG
import pdb

def top_sub_iterdir(start_path, depth, ignorelist=[]):
	"""Same as sub_iterdir but yields the 'start_path' as well.
	"""
	yield start_path
	for subdir in sub_iterdir(start_path, depth-1, ignorelist):
		yield subdir

def sub_iterdir(start_path, depth, ignorelist=[]):
	"""Yields all subdirectories of 'start_path' to a depth specified by
	'depth'. Ignores names in ignorelist.
	"""
	for child in start_path.iterdir():
		if all([child.is_dir(), child.name not in ignorelist]):
			if depth == 0:
				break
			else:
				yield child
				yield from sub_iterdir(child, depth-1)

def tree_block(direc, tagline, header, codeline, args=[]):
	tree = subprocess.check_output(['tree', str(direc)] + args)
	tree = tree.decode('utf-8')
	block = [tagline, header, codeline] + tree.split('\n')[:-3] + [codeline]
	return '\n'.join(block) + '\n'

def find_tree_block(string, tagline, header, codeline):
	try:
		start = string.index(tagline)
		search_offset = start + len('\n'.join([tagline, header, codeline])) + 1
		end = readme_str.index(codeline, search_offset) + len(codeline + '\n')
		return [start, end]
	except:
		raise ValueError("Block not found.")

def is_empty(string):
	if ''.join(string.split()):
		return False
	else:
		return True

if __name__ == "__main__":
	parser = argparse.ArgumentParser(
	           description=__doc__,
	           formatter_class = argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('path', type=str, default='.', nargs='?',
	                    help="Starting path.")
	parser.add_argument('-d', '--depth', type=int, default=0,
	                    help="Number of levels to descend. 0 means all.")
	parser.add_argument('--args', nargs='*', default=['__dirsfirst'],
	                    help="Tree program arguments (except path and -L).\
	                          Instead of '-' use '--'")
	parser.add_argument('--clear', action='store_true',
	                    help="Clear trees from all README.md files.")
	args = parser.parse_args()

	start_path = Path(args.path).resolve()
	if not start_path.exists():
		raise FileNotFoundError("{} does not exist.".format(start_path))
	if not start_path.is_dir():
		raise NotADirectoryError("{} is not a directory.".format(start_path))
	if args.depth < 0:
		raise ValueError("-d (--depth) needs to be more than 0.")
	if '-L' in args.args:
		raise ValueError("Do not include '-L' in --args.")

	tree_args = ['-L', '1'] + [arg.replace('_', '-') for arg in args.args]
	print("tree <directory> {}".format(' '.join(tree_args)))

	tagline = "[//]: # (tree)"
	header = "## Directory tree ##"
	codeline = "~~~~"

	# Do not put trees to READMEs in these folders
	ignorelist = ['.git', '__pycache__']

	print("README.md locations:")
	for direc in top_sub_iterdir(start_path, args.depth, ignorelist):
		block = tree_block(direc, tagline, header, codeline, args=tree_args)
		readme = direc.joinpath("README.md")
		if readme.exists():
			no_file = False
		else:
			no_file = True
		cleared = False
		removed = False
		new_file = False
		new_tree = False
		updated_tree = False
		try:
			# README.md exists
			with open(readme, 'r') as f:
				readme_str = f.read()
			try:
				# README.md has a tree already
				[tree_start, tree_end] = find_tree_block(readme_str,
				                           tagline, header, codeline)
				before = readme_str[:tree_start]
				after = readme_str[tree_end:]
				if args.clear:
					block = ''
					cleared = True
				else:
					updated_tree = True
				new_readme_str = before + block + after
			except ValueError:
				# README.md does not have a tree
				if args.clear:
					block = ''
				else:
					new_tree = True
				new_readme_str = readme_str + block
		except FileNotFoundError:
			# README.md does not exist
			if args.clear:
				block = ''
			else:
				new_file = True
				no_file = False
			new_readme_str = block
		if is_empty(new_readme_str):
			try:
				readme.unlink()
				removed = True
			except FileNotFoundError:
				pass
		else:
			with open(readme, 'w') as f:
				f.write(new_readme_str)

		info = []
		if cleared:
			info.append("(cleared)")
		if removed:
			info.append("(removed)")
		if no_file:
			info.append("(no file)")
		if new_file:
			info.append("(new file)")
		if new_tree:
			info.append("(new tree)")
		if updated_tree:
			info.append("(updated tree)")
		print("{} {}".format(direc, ' '.join(info)))
